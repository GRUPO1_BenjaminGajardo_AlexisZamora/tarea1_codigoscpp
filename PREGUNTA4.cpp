//Escriba una programa que, al recibir como datos 24 n�meros que representan las temperaturas registradas en el
//exterior en un periodo de 24 horas, encuentre, con la ayuda de funciones la temperatura promedio del d�a, as� 
//como la temperatura m�xima y m�nima con el horario en el cual se registraron.

#include <iostream>
#include<conio.h>  // getch()
#include<algorithm> //MAX y MIN
#include<stdlib.h>

using namespace std;

void promedio(float n[24]);		//suma cada uno de los valores del array y los divide en su cantidad (promedio) para luego mostrarlo por pantalla.
void maximo(float n[24], float a[24]);		//devuelve por pantalla el maximo de un array haciendo uso de la funcion std::max de la libreria <algorithm>.
void minimo(float n[24], float a[24]);		//devuelve por pantalla el minimo de un array haciendo uso de la funcion std::min de la libreria <algorithm>
void datos(float n[24], float a[24]);		//Pide los datos (temperaturas) y las guarda en un array tipo float.



int main() {

	float *temp, *horario;									//creacion de puntero
	temp = (float*)calloc(24, sizeof(float));		//le asigna un espacio de memoria para 24 datos tipo float.
	horario = (float*)calloc(24, sizeof(float));
	
	cout<<"Este programa recibe 24 temperaturas en el dia junto al horario en que se registro, y entrega el promedio, maximo y minimo junto al horario en que se registraron estas temperaturas haciendo uso de 4 distintas funciones."<<endl;
	cout<<"cabe destacar que el horario debe estar en formato 24.00 horas (con punto)"<<endl;
	cout<<"Ingrese una por una las 24 temperaturas tomadas durante un dia: "<<endl;
	datos(temp, horario);
	promedio(temp);
	maximo(temp, horario);
	minimo(temp, horario);
	free(temp);		//libera el espacio asignado al puntero.
	free(horario);
	getch();
	}
	
	
	
void promedio(float n[24]){
	unsigned int i;
	float k = 0;
	for (i = 0; i <= 23; i++){
		k = k + n[i];
	}
	k = k / 24;
	cout<<"El promedio de temperaturas es: "<<k<<" grados Celcius"<<endl;
	return;
}
void maximo(float n[24], float a[24]){
	unsigned int i;
	int b = 0;
	float k = n[24];
	for (i = 0; i <= 23; i++){
		k = std::max(k,n[i]);
	}
	for (i = 0; i <=23; i++){
		if (k == n[i]){
			b = i;
		}
	}
	cout<<"La maxima temperatura del dia es de: "<<k<<" grados Celcius y se registro a las "<<a[b]<<" horas."<<endl;
	return;
}
void minimo(float n[24], float a[24]){
	unsigned int i;
	int b = 0;
	float k = n[23];
	for (i = 0; i <= 22; i++){
		k = std::min(k,n[i]);
	}
	
	for (i = 0; i <=23; i++){
	if (k == n[i]){
		b = i;
		}
	}
	cout<<"La minima temperatura del dia es de: "<<k<<" grados Celcius y se registro a las "<<a[b]<<" horas."<<endl;
	return;
}
void datos(float n[24], float a[24]){
	unsigned int i;
	for (i = 0; i <= 23; i++){
		cout<<"Digite la temperatura numero "<<(i+1)<<": ";
		cin>>n[i];
		cout<<"Digite el horario en que se registro esta temperatura: ";
		cin>>a[i];
	}
}
	
	
	

