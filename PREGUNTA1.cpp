#include<iostream>
#include<conio.h>
#include<stdlib.h>
using namespace std;

int funcion(int a, int b);			//Funcion que verifica las condiciones y comprueba que el trabajador este o no capacitado.
int RES;							//Variable global que guarda la respuesta (1  respuesta positiva, 0 respuesta negativa).

int main(){
	int CLA, RES, SAL;
	int *p_CAT, *p_ANT;
	p_CAT = (int*)calloc(1, sizeof(int));				//asignacion de espacio en memoria a los dos punteros.
	p_ANT = (int*)calloc(1, sizeof(int));

	cout<<"Este programa comprueba e imprime si un trabajador reune las condiciones necesarias para el puesto, para ello se pide ingresar variables como: Clave, Categoria, Antiguedad y Salario"<<endl;
	cout<<"Ingresar clave del trabajador: "<<endl;
	cin>>CLA;
	cout<<"Ingresar categoria del trabajador: "<<endl;
	cin>>*p_CAT;
	cout<<"Ingresar antiguedad del trabajador: "<<endl;
	cin>>*p_ANT;
	cout<<"Ingresar salario del trabajador: "<<endl;
	cin>>SAL;
	
	funcion(*p_CAT, *p_ANT);
	free(p_CAT);
	free(p_ANT);							//se libera el espacio utilizado por los punteros.
	getch();
}
	
	int funcion(int a, int b){
	
	if (a == 3 || a == 4) {					//si categoria es 3 o 4
		 if (b > 5){						//y la antiguedad es mayor a 5 a�os, trabajador capacitado.
			RES = 1;
			cout<<"El trabajador esta capacitado. RES = "<<RES;	
			getch();
			return 1;
			}
			}								//o
	
	if (a >= 2){							//si categoria es mayor a 2
		if(b > 7){							//y la antiguedad mayor a 7 a�os, trabajador capacitado.
			RES = 1;
			cout<<"El trabajador esta capacitado. RES = "<<RES;
			getch();
			return 2;
			}
		}

	RES = 0;								//cualquier otro caso, trabajador no capacitado.
	cout<<"El trabajador no esta capacitado. RES = "<<RES;
	getch();
	return 0;
	}
    
