#include <iostream>
#include <time.h>  // para la funcion srand(time(null))
#include<conio.h>  // getch()
#include <stdlib.h>

using namespace std;


void Binario(int n[255]); //Declaracion de la funcion, esta recibe un array de 255 numeros enteros, y devuelve por pantalla cada numero en el sistema binario.
int random(int a[255]);  //Declaracion de la funcion, esta recibe un array de 255 numeros enteros, y devuelve el array con numeros aleatorios desde 0 a 255.

int main(){
	int *X; 			// Puntero tipo entero que almacenara los 255 numeros enteros.
	X = (int*)calloc(255, sizeof(int)); 	 //guarda espacio en memoria para 255 valores del tipo entero.
	cout<<"Este programa en un inicio genera 256 valores aleatorios en el rango [0, 255], ";
	cout<<"luego toma cada uno de los valores y los presenta en el sistema binario."<<endl;
	cout<<"Presione enter para continuar."<<endl;
	getch();
	
	random(X);

	cout<<"Presione enter para ver los valores obtenidos en el sistema binario"<<endl;
	getch();

	Binario(X);					
	free(X);			//Libera el espacio reservado por calloc.
	getch();	
}

void Binario(int n[255]){
	int c, k;
	unsigned int i;
	for (i = 0; i < 255; i++){
		cout<<""<<endl;
		cout<<"Numero " <<i<<" en sistema binario: ";
		
		for (c = 7; c >= 0; c--){			//algotirmo para pasar de decimal a binario.
			k = k + n[i] >> c;
			if (k & 1){
				cout<<"1 ";
						}
			else {
				cout<<"0 ";
		}
		}
	}
}

int random(int a[255]){
	unsigned int i;
	srand(time(NULL));  //declaracion necesaria para que rand() devuelva siempre numeros aleatorios distintos cada vez que se ejecuta.
	
	for (i = 0; i <= 255; i++){   //for para recorrer cada espacio del array para generar los numeros aleatorios.
		
		a[i] = rand() % 256;		// (%) limita el rango de numeros aleatorios a [0, 255].
		cout<<"Numero aleatorio "<<i<<" : "<<a[i]<<endl;
		}
}


