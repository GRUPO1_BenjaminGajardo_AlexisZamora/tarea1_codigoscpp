#include<iostream>
#include<conio.h>  // getch()
#include<stdlib.h>

using namespace std;

int funcion(int a); 			//Funcion que recibe el numero de silbidos y devuelve por pantalla la temperatura en grados Celsius (declaracion).

int main(){
	
	int *S;
	S = (int*)calloc(1, sizeof(int));
	
	cout<<"Este programa toma el numero de silbidos de una chicharra durante 1 minuto para calcular la temperatura en grados Celcius [C]"<<endl;
	cout<<"Escuche atentamente una chicharra e ingrese el numero de silbidos en 1 minuto a continuacion: "<<endl;
	cin>>*S;  					//pide el numero de silbidos
	funcion(*S);
	free(S);  					//libera el espacio en memoria utilizado por S.
	getch();
}


int funcion(int a){
	int FA, C;
		FA = (a/4) + 40;		//Ecuacion que calcula la temperatura en base a la cantidad de silbidos.
		C = (FA-32) / 1.8;
		cout<<"La temperatura es " <<C<<" grados Celsius.";
}
