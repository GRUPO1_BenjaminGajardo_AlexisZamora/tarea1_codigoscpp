//construya un programa que, al recibir un arreglo unidimensional de tipo entero que contiene calificaciones de ex�menes de alumno, calcule lo siguiente
//Varianza, media aritmetica, desviacion estandar, moda
//Dato: ALU[N] (arreglo unidimensional de tipo entero de N elementos, 1 <= N <= 100).

#include <iostream>
#include<conio.h>  // getch()
#include<stdlib.h> 
#include <math.h>					//Libreria para usar pow().

using namespace std;

void media(int a[]);				//funcion que devuelve la media de un arreglo.
void desviacionyvarianza(int a[]);	//Funcion que devuelve la desviacion y varianza de un arreglo, se utiliza una funcion para estos dos parametros por la relacion que tienen.
void moda(int a[]);					//Funcion que devuelve la moda de un arreglo.
void pedirdatos(int a, int b[]);	//Funcion para pedir datos y guardarlos en un arreglo.
int N;								//variable global (porque la usan las funciones) que guarda la cantidad de calificaciones.

int main() {
	int *calif;						//Variable puntero para guardar las calificaciones.
	
	cout<<"Este programa recibe un arreglo unidimensional de tipo entero con calificaciones [0, 100], para luego mostrar por pantalla la media aritmetica, varianza, desviacion estandar y la moda haciendo uso de distintas funciones y punteros."<<endl;
	cout<<"ingrese el numero de calificaciones del array: ";
	cin>>N;
	
	calif = (int*)calloc(N, sizeof(int));		//Asignacion de espacio en memoria para *calif.
	pedirdatos(N, calif);
	media(calif);
	desviacionyvarianza(calif);
	moda(calif);
	getch();
}

void pedirdatos(int a, int b[]) {
	unsigned int i;
	for (i = 0; i < N; i++){										//ciclo para pedir datos
		cout<<"ingrese la calificacion numero "<<(i+1)<<": ";
		cin>>b[i];
		}
	}


void media(int a[]){
	int b = 0;
	unsigned int i;
	
	for (i = 0; i < N; i++){					//Ciclo para sumar todos los valores del arreglo y dividirlos en la cantidad de estos (media).
		b = b + a[i];
	}
	
	b = b / N;
	cout<<"La media aritmetica del arreglo es: "<<b<<endl;
}

void moda(int a[]){
	
	int i, k;
	int b, s, m = 0;
   for(i=0;i<N;i++){						//ciclo que compara una calificacion con todas las demas.
        s=0;
        for(k=0;k<N;k++){					//ciclo que va cambiando la comparacion anterior.
            if(a[i]==a[k] && i!=k){
                s=s+1;						//Contador para obtener la cantidad de veces que se repite una calificacion.
            }               
        }
        if(s>=m){							//condicional que guarda el valor con la mayor cantidad de repeticiones.
        m=s;								
        b=i;    
        }
    }

    cout<<"la moda es: "<<a[b]<<" y tiene "<<m+1<<" repeticiones"<<endl;

}
  

void desviacionyvarianza(int a[]){
	int media;
	float b = 0;				//float para la desviacion y varianza.
	unsigned int i;
	
	for (i = 0; i < N; i++){	//ciclo que calcula la media aritmetica del arreglo.
	media = media + a[i];
	}
	
	media = media / N;			//se obtiene la media.
	
	for (i = 0; i < N; i++) {				//ciclo que calcula la desviacion estandar.
		b = b + pow((a[i] - media), 2);
	}
	
	b = b / (N - 1);						//se obtiene la desviacion.
	cout<<"La desviacion estandar del arreglo es de: "<<b<<endl;
	
	b = pow(b, 2);							//la varianza es el cuadrado de la desviacion estandar.
	
	cout<<"La varianza del arreglo es de "<<b<<endl;
}
